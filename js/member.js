/*
 * Copyright 2018 Tom Kranz
 *
 * This file is part of RegApp.
 *
 * RegApp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RegApp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RegApp.  If not, see <https://www.gnu.org/licenses/>.
 */

var presenter;
var automaton;
var utilities;

reglibjs().then(Module => utilities = new Utilities(Module));

function solution(member) {
  if (member) {
    presenter.proposeMembership();
  } else {
    presenter.rejectMembership();
  }
}

function generateWord() {
  let a = automaton.getAlphabet();
  let length = Math.floor(-Math.log(1-Math.random())/-Math.log(5./6.));
  let word = "";
  for (var i = 0; i < length; i++) {
    word += a.get(Math.floor(Math.random()*a.size()));
  }
  a.delete();
  presenter.begin(word);
  document.getElementById("footer").style.visibility = "visible";
  document.getElementById("action_nonmember").classList.remove("notyetvisibile");
  document.getElementById("action_member").classList.remove("notyetvisibile");
}

function readFile(files) {
  var file = files[0];
  if (!file) {
    return;
  }
  var reader = new FileReader();
  reader.addEventListener("load", (eve) => {
    spec = JSON.parse(eve.target.result);
    if (spec.handwritten) {
      let builder = utilities.readAutomaton(spec.handwritten);
      automaton = builder.buildNFA();
      builder.delete()
    } else if (spec.graph) {
      automaton = utilities.elementsToAutomaton(spec.graph, true);
    } else if (spec.re) {
      let re = utilities.Module.REFromString(spec.re);
      automaton = re.asNFA();
      re.delete();
    } else {
      throw "No solution in input file";
    }
    let toast = document.getElementById("toast").MaterialSnackbar;
    toast.hideSnackbar();
    presenter = new MembershipPresenter(
      new GraphView(
        document.getElementById("paper"),
        document.getElementById("footer_left").children[1].children,
        document.getElementById("footer_right").children,
        document.getElementById("action_member"),
        document.getElementById("action_nonmember"),
        toast,
        utilities.elementsOfAutomaton(automaton),
        utilities.defaultStyle(),
        utilities.Module
      ),
      new MembershipModel(automaton, utilities.Module),
      utilities.Model
    );
    document.getElementById("action_load").classList.add("notyetvisibile");
    document.getElementById("action_help").classList.remove("notyetvisibile");
    document.getElementById("action_generate").classList.remove("notyetvisibile");
  });
  reader.readAsText(file);
}

class GraphView {
  constructor(container, wordgroup, statusgroup, member, nonmember, toast, eles, style, rljs) {
    this.graph = cytoscape({
      container: container,
      style: style,
      elements: eles,
      layout : {name: "cose"}
    });
    this.graph.elements().unselectify();
    this.graph.$(".initial").removeClass("initial");
    this.done = wordgroup[0];
    this.current = wordgroup[1];
    this.ahead = wordgroup[2];
    this.pastStatus = statusgroup[0];
    this.wantedStatus = statusgroup[1];
    this.member = member;
    this.nonmember = nonmember;
    this.toast = toast;
    this.rljs = rljs;
    this.hideMemberButton = this.hideMemberButton.bind(this);
    this.hideNonmemberButton = this.hideNonmemberButton.bind(this);
    this.dispatchMessage = this.dispatchMessage.bind(this);
    this.words = this.words.bind(this);
    this.status = this.status.bind(this);
    this.futures = this.futures.bind(this);
    this.pasts = this.pasts.bind(this);
    this.select = this.select.bind(this);
    this.setCallbacks = this.setCallbacks.bind(this);
  }

  hideMemberButton(hide) {
    if (hide) {
      this.member.classList.add("notyetvisibile");
    } else {
      this.member.classList.remove("notyetvisibile");
    }
  }

  hideNonmemberButton(hide) {
    if (hide) {
      this.nonmember.classList.add("notyetvisibile");
    } else {
      this.nonmember.classList.remove("notyetvisibile");
    }
  }

  dispatchMessage(messageObject) {
    if (this.toast.active) {
      this.toast.hideSnackbar();
      setTimeout(() => this.toast.showSnackbar(messageObject), this.toast.Constant_.ANIMATION_LENGTH);
    } else {
      this.toast.showSnackbar(messageObject);
    }
  }

  words(done, current, ahead) {
    this.done.innerHTML = "";
    let doneNode = document.createTextNode(done);
    this.done.appendChild(doneNode);
    this.current.innerHTML = "";
    let currentText = (current.length ? current : this.rljs.REGetEmptyString());
    let currentNode = document.createTextNode(currentText);
    this.current.appendChild(currentNode);
    this.ahead.innerHTML = "";
    let aheadNode = document.createTextNode(ahead);
    this.ahead.appendChild(aheadNode);
  }

  status (past, wanted) {
    this.pastStatus.children[1].innerHTML = "";
    this.pastStatus.style.visibility = (past ? "visible" : "hidden");
    let pastNode = document.createTextNode(past);
    this.pastStatus.children[1].appendChild(pastNode);
    this.wantedStatus.children[1].innerHTML = "";
    let wantedNode = document.createTextNode(wanted);
    this.wantedStatus.children[1].appendChild(wantedNode);
    this.wantedStatus.style.visibility = (wanted ? "visible" : "hidden");
  }

  futures(states) {
    this.graph.startBatch();
    this.graph.$(".future")
        .removeClass("future");
    states.forEach(state =>
      this.graph.$('node[label="'+state+'"]')
          .addClass("future")
    );
    this.graph.endBatch();
  }

  pasts(states) {
    this.graph.startBatch();
    this.graph.$(".past")
        .removeClass("past");
    states.forEach(state =>
      this.graph.$('node[label="'+state+'"]')
          .addClass("past")
    );
    this.graph.endBatch();
  }

  select(state) {
    this.graph.startBatch();
    this.graph.elements().selectify().unselect();
    if (state !== null) {
      this.graph.$('node[label="'+state+'"]').select();
    }
    this.graph.elements().unselectify();
    this.graph.endBatch();
  }

  setCallbacks(state, transition) {
    let resizeTimeoutId;
    window.addEventListener("resize", (event) => {
      clearTimeout(resizeTimeoutId);
      resizeTimeoutId = setTimeout(() => {
          this.graph.animate({center:{}, fit:{padding:30}});
        },
        250
      );
    }, false);
    this.graph.on('tap', 'node', event => state(event.target.data().label));
  }
}

class MembershipModel {
  constructor(automaton, rljs) {
    this.automaton = automaton;
    this.rljs = rljs;
    this.ahead = "";
    this.current = "";
    this.done = "";
    this.present = null;
    this.future = [];
    this.past = [];
    this.wanted = [];
    this.finished = false;
    this.accepted = this.accepted.bind(this);
    this.initialize = this.initialize.bind(this);
    this.moveOn = this.moveOn.bind(this);
    this.selectState = this.selectState.bind(this);
    this.next = this.next.bind(this);
  }

  accepted() {
    let reached = this.automaton.deltaHat(
        this.automaton.getInitialState(),
        this.done + this.current + this.ahead
      );
    if (this.automaton instanceof this.rljs.NFA) {
      let result = this.automaton.isAcceptingSet(reached);
      reached.delete();
      return result;
    } else {
      return this.automaton.isAccepting(reached);
    }
  }

  initialize(word) {
    this.past = [];
    this.future = [];
    this.wanted = [];
    this.present = this.automaton.getInitialState();
    this.past.push(this.present);
    this.future.push(this.present);
    if (this.automaton instanceof this.rljs.NFA) {
      let ec = this.automaton.epsilonClosure(this.present);
      for (var i = 0; i < ec.size(); i++) {
        let q = ec.get(i);
        if (q !== this.present) {
          this.wanted.push(q);
        }
      }
      ec.delete();
    }
    this.finished = false;
    this.done = "";
    this.current = "";
    this.ahead = word;
  }

  moveOn() {
    if (!this.finished && this.present !== null && this.wanted.length == 0) {
      let i = this.past.indexOf(this.present);
      if (i > -1) {
        this.past.splice(i, 1);
      }
      this.present = null;
      return true;
    }
    return false;
  }

  selectState(state) {
    if (this.finished) {
      let i = this.past.indexOf(state);
      if (i > -1) {
        if (!this.wanted.includes(state)) {
          this.past.splice(i, 1);
        } else if (this.wanted.length == this.past.length){
          this.wanted = [];
          this.past = [];
          this.present = state;
        }
      }
    } else if (this.present === null && this.past.includes(state)) {
      this.present = state;
      let reached = this.automaton.deltaHat(state, this.current);
      if (this.automaton instanceof this.rljs.NFA) {
        for (var i = 0; i < reached.size(); i++) {
          let q = reached.get(i);
          if (!this.future.includes(q)) {
            this.wanted.push(q);
          }
        }
        reached.delete();
      } else {
        this.wanted.push(reached);
      }
    } else if (this.present !== null && !this.future.includes(state)) {
      let i = this.wanted.indexOf(state);
      if (i > -1) {
        this.wanted.splice(i, 1);
        this.future.push(state);
      }
    }
    return this.present;
  }

  next() {
    if (this.past.length == 0){
      this.past = this.future;
      this.future = [];
      let c = this.ahead[Symbol.iterator]().next();
      this.done += this.current;
      this.finished = c.done;
      this.current = (c.done ? "" : c.value);
      this.ahead = this.ahead.replace(this.current, "");
      if (!this.finished && this.past.length == 1) {
        this.selectState(this.past[0]);
        this.moveOn() && this.next();
      }
      if (this.finished) {
        this.wanted = [];
        let states = this.automaton.getStates();
        for (let i = 0; i < states.size(); i++) {
          let q = states.get(i);
          if (this.automaton.isAccepting(q) && this.past.includes(q)) {
            this.wanted.push(q);
          }
        }
        states.delete();
      }
      return true;
    } else {
      return false;
    }
  }
}

class MembershipPresenter {
  constructor(view, model, rljs) {
    this.view = view;
    this.model = model;
    this.rljs = rljs;
    this.proposeMembership = this.proposeMembership.bind(this);
    this.rejectMembership = this.rejectMembership.bind(this);
    this.checkProgress = this.checkProgress.bind(this);
    this.selectState = this.selectState.bind(this);
    this.begin = this.begin.bind(this);
    this.view.setCallbacks(this.selectState);
    this.view.dispatchMessage({message: _("membership_lets_go")});
  }

  proposeMembership() {
    if (this.model.accepted()) {
      this.view.dispatchMessage({message: _("membership_correct_member")});
    } else {
      this.view.dispatchMessage({message: _("membership_wrong_nonmember")});
    }
  }

  rejectMembership() {
    if (this.model.accepted()) {
      this.view.dispatchMessage({message: _("membership_wrong_member")});
    } else {
      this.view.dispatchMessage({message: _("membership_correct_nonmember")});
    }
  }

  checkProgress() {
    if (this.model.moveOn()) {
      if (this.model.next()) {
        if (this.model.finished) {
          this.view.dispatchMessage({message: _("membership_status_finished"), timeout: 2*2750});
        } else if (!this.model.past.length) {
          this.view.dispatchMessage({message: _("membership_status_nowhere_to_go"), timeout: 1.5*2750});
          this.view.hideMemberButton(true);
        }
      }
    }
  }

  selectState(state) {
    this.model.selectState(state);
    this.checkProgress();
    if (this.model.finished) {
      if (this.model.present !== null) {
        this.view.hideNonmemberButton(true);
      } else if (!this.model.past.length) {
        this.view.hideMemberButton(true);
      }
    }
    this.view.pasts(this.model.past);
    this.view.futures(this.model.future);
    this.view.select(this.model.present);
    this.view.words(this.model.done, this.model.current, this.model.ahead);
    this.view.status(this.model.past.length, (this.model.finished ? 0 : this.model.wanted.length));
  }

  begin(word) {
    this.model.initialize(word);
    this.checkProgress();
    this.view.pasts(this.model.past);
    this.view.futures(this.model.future);
    this.view.select(this.model.present);
    this.view.words(this.model.done, this.model.current, this.model.ahead);
    this.view.status(this.model.past.length, (this.model.finished ? 0 : this.model.wanted.length));
  }
}
