/*
 * Copyright 2018 Tom Kranz
 *
 * This file is part of RegApp.
 *
 * RegApp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RegApp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RegApp.  If not, see <https://www.gnu.org/licenses/>.
 */

var presenter;
var utilities;
var toast;
var tutorialButton;
var cancelTutorialButton;

reglibjs().then(Module=>utilities = new Utilities(Module));

function readFile(files) {
  let automaton;
  var file = files[0];
  if (!file) {
    return;
  }
  var reader = new FileReader();
  reader.addEventListener("load", (eve) => {
    spec = JSON.parse(eve.target.result);
    if (spec.handwritten) {
      let builder = utilities.readAutomaton(spec.handwritten);
      automaton = builder.buildNFA();
      builder.delete()
    } else if (spec.graph) {
      automaton = utilities.elementsToAutomaton(spec.graph, true);
      if (automaton instanceof utilities.Module.DFA) {
        let nfa = automaton.asNFA();
        automaton.delete();
        automaton = nfa;
      }
    } else if (spec.re) {
      let re = utilities.Module.REFromString(spec.re);
      automaton = re.asNFA();
      let builder = new utilities.Module.FABuilder(automaton);
      builder.powerset();
      builder.minimize();
      builder.normalizeStateNames("");
      automaton.delete();
      automaton = builder.buildNFA();
      re.delete();
    } else {
      throw "No solution in input file";
    }
    presenter = new StateElimPresenter(
      new StateElimModel(new utilities.Module.GNFAFromNFA(automaton)),
      new ElimGraphView(document.getElementById("paper"), document.getElementById("re_dialog"))
    );
    toast = document.getElementById("toast").MaterialSnackbar;
    cancelTutorialButton = document.getElementById("action_cancel_tutorial");
    tutorialButton = document.getElementById("action_tutorial");
    toast.hideSnackbar();
    tutorialButton.classList.remove("notyetvisibile");
    document.getElementById("action_load").classList.add("notyetvisibile");
  });
  reader.readAsText(file);
}

function inputAppend(which) {
  let input = document.getElementById("re_dialog_re");
  let restring = input.value;
  if (which === "null") {
    input.parentElement.MaterialTextfield.change(restring+utilities.Module.REGetEmptySet());
  } else if (which === "empty") {
    input.parentElement.MaterialTextfield.change(restring+utilities.Module.REGetEmptyString());
  }
  input.focus();
  input.parentElement.MaterialTextfield.checkFocus();
}

class StateElimModel {
  constructor(gnfa) {
    this.gnfa = gnfa;
    this.ins = [];
    this.left = null;
    this.center = null;
    this.right = null;
    this.outsleft = [];
    this.outs = [];
    this.offers = null;
    this.map = {};
    this.rev = {};
    this.updateMap = this.updateMap.bind(this);
    this.offerRE = this.offerRE.bind(this);
    this.selectState = this.selectState.bind(this);
    this.updateMap();
  }

  updateMap() {
    let initial = this.gnfa.getInitialState();
    let final = this.gnfa.getAcceptingState();
    this.map = {[initial]: {}, [final]: {}};
    this.rev = {[initial]: {}, [final]: {}};
    let re = this.gnfa.getTransition(initial, final);
    if (re.getOperation() !== utilities.Module.REOperation.empty) {
      this.map[initial][final] = re.toString();
      this.rev[final][initial] = re.toString();
    }
    re.delete()
    let activeStates = this.gnfa.getActiveStates();
    for (let i = 0; i < activeStates.size(); i++) {
      let q = activeStates.get(i);
      this.map[q] = {};
      this.rev[q] = {};
      re = this.gnfa.getTransition(initial, q);
      if (re.getOperation() !== utilities.Module.REOperation.empty) {
        this.map[initial][q] = re.toString();
        this.rev[q][initial] = re.toString();
      }
      re.delete();
      re = this.gnfa.getTransition(q, final);
      if (re.getOperation() !== utilities.Module.REOperation.empty) {
        this.map[q][final] = re.toString();
        this.rev[final][q] = re.toString();
      }
      re.delete();
      for (let j = 0; j < activeStates.size(); j++) {
        let p = activeStates.get(j);
        re = this.gnfa.getTransition(q, p);
        if (re.getOperation() !== utilities.Module.REOperation.empty) {
          this.map[q][p] = re.toString();
        }
        re.delete();
        re = this.gnfa.getTransition(p, q);
        if (re.getOperation() !== utilities.Module.REOperation.empty) {
          this.rev[q][p] = re.toString();
        }
        re.delete();
      }
    }
    activeStates.delete();
  }

  selectState(state) {
    if (this.center !== null) {
      if (this.left === null && this.ins.includes(state)) {
        this.left = state;
        this.outsleft = this.outs.slice();
      } else if (this.right === null && this.outsleft.includes(state)) {
        this.right = state;
      } else {
        return false;
      }
    } else if (state in this.map && state !== this.gnfa.getAcceptingState() &&
               state in this.rev && state !== this.gnfa.getInitialState()) {
      this.center = state;
      this.ins = Object.keys(this.rev[state]);
      let i = this.ins.indexOf(state);
      if (i > -1) {
        this.ins.splice(i, 1);
      }
      this.outs = Object.keys(this.map[state]);
      i = this.outs.indexOf(state);
      if (i > -1) {
        this.outs.splice(i, 1);
      }
      if (!this.ins.length || !this.outs.length) {
        this.ins = [];
        this.center = null;
        this.outs = [];
        this.gnfa.ripState(state);
        this.updateMap();
      } else {
        this.offers = new utilities.Module.JSStringJSStringExpressionMapMap();
        this.outs.forEach(target=>{
          let targetMap = new utilities.Module.JSStringExpressionMap();
          this.offers.set(target, targetMap);
          targetMap.delete();
        });
      }
    } else {
      return false;
    }
    return true;
  }

  offerRE(re) {
    let success = false;
    if (!(this.right === null || this.left === null || this.center === null)) {
      let offer = utilities.Module.REFromString(re);
      let lc = this.gnfa.getTransition(this.left, this.center);
      let cc = this.gnfa.getTransition(this.center, this.center);
      let cr = this.gnfa.getTransition(this.center, this.right);
      let lr = this.gnfa.getTransition(this.left, this.right);
      let actual = utilities.Module.REAlternation(
          lr,
          utilities.Module.REConcatenation(
              utilities.Module.REConcatenation(lc, utilities.Module.REKleene(cc)),
              cr
          )
      );
      if (actual.equals(offer)) {
        let targetMap = this.offers.get(this.right);
        targetMap.set(this.left, offer);
        this.offers.set(this.right, targetMap);
        targetMap.delete();
        let i = this.outsleft.indexOf(this.right);
        if (i > -1) {
          this.outsleft.splice(i, 1);
        }
        this.right = null;
        if (!this.outsleft.length) {
          i = this.ins.indexOf(this.left);
          if (i > -1) {
            this.ins.splice(i, 1);
          }
          this.left = null;
          if (!this.ins.length) {
            this.gnfa.ripStateOffer(this.center, this.offers);
            this.offers.delete();
            this.outs = [];
            this.center = null;
            this.updateMap();
          }
        }
        success = true;
      }
      offer.delete();
      lc.delete();
      cc.delete();
      cr.delete();
      lr.delete();
      actual.delete();
    }
    return success;
  }
}

class ElimGraphView {
  constructor(graphcontainer, redialog) {
    this.setCallbacks = this.setCallbacks.bind(this);
    this.highlightInitial = this.highlightInitial.bind(this);
    this.highlightAccepting = this.highlightAccepting.bind(this);
    this.highlightRemovable = this.highlightRemovable.bind(this);
    this.highlightCentral = this.highlightCentral.bind(this);
    this.highlightIncoming = this.highlightIncoming.bind(this);
    this.highlightOutgoing = this.highlightOutgoing.bind(this);
    this.queryRE = this.queryRE.bind(this);
    this.drawOverview = this.drawOverview.bind(this);
    this.drawDetail = this.drawDetail.bind(this);

    // Not part of the interface
    this._graph = cytoscape({
      container: graphcontainer,
      layout: {name: "cose", animate: "end"},
      style: utilities.defaultStyle(),
      autounselectify: true,
      userZoomingEnabled: false,
      userPanningEnabled: false,
      boxSelectionEnabled: false
    });
    this._redialog = redialog;
    this._overview = false;
    this._highlight = this._highlight.bind(this);
  }

  setCallbacks(state, re) {
    let resizeTimeoutId;
    window.addEventListener("resize", (event) => {
      clearTimeout(resizeTimeoutId);
      resizeTimeoutId = setTimeout(() => {
          this._graph.animate({center:{}, fit:{padding:30}});
        },
        250
      );
    }, false);
    this._graph.on("tap", "node", eve=>state(eve.target.data("label")));
    let input = this._redialog.querySelector("input");
    let error = this._redialog.querySelector("span");
    this._redialog.querySelector("#re_dialog_submit").onclick = () => {
      if (input.checkValidity()) {
        let restring = input.value;
        try {
          if (re(restring)) {
            this._redialog.close();
          } else {
            error.innerText = (_("elim_action_re_wrong"));
            input.value = "";
            input.parentElement.MaterialTextfield.checkValidity();
            input.value = restring;
          }
        } catch (e) {
          utilities.Module.extractMessageInvalidArgument(e);
          error.innerText = (_("elim_action_re_malformed"));
          input.value = "";
          input.parentElement.MaterialTextfield.checkValidity();
          input.value = restring;
        }
      } else {
        error.innerText = (_("elim_action_re_empty"));
      }
    };
  }

  highlightInitial(condition, interval) {
    this._highlight('.initial', condition, interval);
  }

  highlightAccepting(condition, interval) {
    this._highlight('.accepting', condition, interval);
  }

  highlightRemovable(condition, interval) {
    this._highlight(e => !(e.hasClass('initial') || e.hasClass('accepting')), condition, interval);
  }

  highlightCentral(condition, interval) {
    this._highlight(e => e.data("col") == 1, condition, interval);
  }

  highlightIncoming(condition, interval) {
    this._highlight(e => e.data("col") == 0, condition, interval);
  }

  highlightOutgoing(condition, interval) {
    this._highlight(e => e.data("col") == 2, condition, interval);
  }

  queryRE(left, center, right, map) {
    this._redialog.querySelector("span").innerText = (_("elim_action_re_empty"));
    let input = this._redialog.querySelector("input");
    input.parentElement.MaterialTextfield.change("");
    let button = this._redialog.querySelector("#re_dialog_empty");
    button.style.textTransform = "none";
    button.innerText = utilities.Module.REGetEmptyString();
    button = this._redialog.querySelector("#re_dialog_null");
    button.style.textTransform = "none";
    button.innerText = utilities.Module.REGetEmptySet();
    let hints = this._redialog.querySelector("#re_dialog_hints").children;
    hints[0].innerText = left+"→"+center+": "+map[left][center];
    hints[1].innerText = (center in map[center] ? "↺"+center+": "+map[center][center] : "");
    hints[2].innerText = center+"→"+right+": "+map[center][right];
    hints[3].innerText = (right in map[left] ? left+"→"+right+": "+map[left][right] : "");
    this._redialog.querySelector("label").innerText = left+"→"+right+"?";
    this._redialog.showModal();
    input.focus();
    input.parentElement.MaterialTextfield.checkFocus();
  }

  drawOverview(map, initial, final) {
    if (this._overview) {
      this._graph.nodes().filter(ele => !(ele.data().label in map)).remove();
      this._graph.layout({name: "cose", animate: "end"}).run();
    } else {
      this._overview = true;
      this._graph.startBatch();
      this._graph.elements().remove();
      for (let q in map) {
        let clz = [];
        if (q === initial) clz.push("initial");
        if (q === final) clz.push("accepting");
        this._graph.add({group: "nodes", data: {id: q, label: q}, classes: clz.join(" ")});
      }
      for (let [q, pr] of Object.entries(map)) {
        for (let [p, r] of Object.entries(pr)) {
          this._graph.add({
              group: "edges",
              data: {
                  id: q+"-"+r+">"+p,
                  label: r,
                  source: q,
                  target: p
                }
            });
        }
      }
      this._graph.nodes().unlock();
      this._graph.endBatch();
      this._graph.layout({name: "cose", animate: "end"}).run();
    }
  }

  drawDetail(map, ins, center, outs) {
    let rows = Math.max(ins.length, outs.length);
    this._overview = false;
    this._graph.startBatch();
    this._graph.elements().remove();
    this._graph.add({group: "nodes", data: {id: "c"+center, label: center, row: rows/2., col: 1}});
    if (center in map[center]) {
      this._graph.add({
          group: "edges",
          data: {
              id: "c"+center+"-"+map[center][center]+">"+"c"+center,
              label: map[center][center],
              source: "c"+center,
              target: "c"+center
            }
        });
    }
    ins.forEach((q, i) => {
      this._graph.add({group: "nodes", data: {id: "l"+q, label: q, row: rows*(ins.length == 1 ? 1./2. : i/(ins.length-1.)), col: 0}});
      this._graph.add({
          group: "edges",
          data: {
              id: "l"+q+"-"+map[q][center]+">"+"c"+center,
              label: map[q][center],
              source: "l"+q,
              target: "c"+center
            }
        });
    });
    outs.forEach((p, i) => {
      this._graph.add({group: "nodes", data: {id: "r"+p, label: p, row: rows*(outs.length == 1 ? 1./2. : i/(outs.length-1.)), col: 2}});
      this._graph.add({
          group: "edges",
          data: {
              id: "c"+center+"-"+map[center][p]+">"+"r"+p,
              label: map[center][p],
              source: "c"+center,
              target: "r"+p
            }
        });
      ins.forEach((q, j)=>{
        if (p in map[q]) {
          this._graph.add({
              group: "edges",
              data: {
                  id: "l"+q+"-"+map[q][p]+">"+"r"+p,
                  label: map[q][p],
                  source: "l"+q,
                  target: "r"+p
                },
                style: {"curve-style": "unbundled-bezier", "control-point-distances": j < ins.length/2?-100:100, "control-point-weights": 0.5}
            });
        }
      });
    });
    this._graph.endBatch();
    this._graph.nodes().unlock();
    this._graph.layout({name: "grid", rows: rows, cols: 3, position: node=>{return {row: node.data("row"), col: node.data("col")}}}).run();
    this._graph.nodes().lock();
  }

  _highlight(selector, condition, interval, unhighlight) {
    if (typeof interval !== 'number') {
      interval = 500;
    }
    let selected = this._graph.nodes(selector);
    if (unhighlight) {
      selected.removeClass('highlighted');
      setTimeout(() => this._highlight(selector, condition, interval), interval);
    } else if (condition()) {
      selected.addClass('highlighted');
      setTimeout(() => this._highlight(selector, condition, interval, true), interval);
    }
  }
}

class StateElimPresenter {
  constructor(model, view) {
    this.model = model;
    this.view = view;
    this.advanceTutorial = this.advanceTutorial.bind(this);
    this.cancelTutorial = this.cancelTutorial.bind(this);
    this.selectState = this.selectState.bind(this);
    this.submitRE = this.submitRE.bind(this);
    this.view.setCallbacks(this.selectState, this.submitRE);
    this.view.drawOverview(this.model.map, this.model.gnfa.getInitialState(), this.model.gnfa.getAcceptingState());

    // Not part of the interface
    this._tutorialStep = 0;
  }

  cancelTutorial() {
    this._tutorialStep = 0;
    toast.hideSnackbar();
    tutorialButton.classList.remove("notyetvisibile");
    cancelTutorialButton.classList.add("notyetvisibile");
  }

  advanceTutorial() {
    this._tutorialStep = (this._tutorialStep + 1) % 8;
    tutorialButton.classList.add("notyetvisibile");
    cancelTutorialButton.classList.remove("notyetvisibile");
    switch (this._tutorialStep) {
      case 0:
        toast.hideSnackbar();
        tutorialButton.classList.remove("notyetvisibile");
        cancelTutorialButton.classList.add("notyetvisibile");
        break;
      case 1:
        if (Object.keys(this.model.map).length <= 2) {
          this._tutorialStep = 6;
          return this.advanceTutorial();
        }
        if (this.model.center !== null) {
          return this.advanceTutorial();
        }
        toast.hideSnackbar();
        toast.showSnackbar({
            message: _("elim_action_tutorial_unremovable"),
            timeout: 2**31-1,
            actionText: _("elim_action_tutorial_continue"),
            actionHandler: this.advanceTutorial
        });
        this.view.highlightInitial(() => this._tutorialStep === 1);
        this.view.highlightAccepting(() => this._tutorialStep === 1);
        break;
      case 2:
        if (Object.keys(this.model.map).length <= 2) {
          this._tutorialStep = 6;
          return this.advanceTutorial();
        }
        if (this.model.center !== null) {
          return this.advanceTutorial();
        }
        toast.hideSnackbar();
        this.view.highlightRemovable(() => this._tutorialStep === 2);
        toast.showSnackbar({
            message: _("elim_action_tutorial_removable"),
            timeout: 2**31-1
        });
        break;
      case 3:
        if (this.model.center === null) {
          this._tutorialStep = 1;
          return this.advanceTutorial();
        }
        if (this.model.left !== null) {
          return this.advanceTutorial();
        }
        this.view.highlightCentral(() => this._tutorialStep === 3);
        toast.hideSnackbar();
        toast.showSnackbar({
          message: _("elim_action_tutorial_selected"),
          timeout: 2**31-1,
          actionText: _("elim_action_tutorial_continue"),
          actionHandler: this.advanceTutorial
        });
        break;
      case 4:
        if (this.model.left !== null) {
          return this.advanceTutorial();
        }
        this.view.highlightIncoming(() => this._tutorialStep === 4);
        toast.hideSnackbar();
        toast.showSnackbar({
          message: _("elim_action_tutorial_incoming"),
          timeout: 2**31-1
        });
        break;
      case 5:
        if (this.model.right !== null) {
          return this.advanceTutorial();
        }
        this.view.highlightOutgoing(() => this._tutorialStep === 5);
        toast.hideSnackbar();
        toast.showSnackbar({
          message: _("elim_action_tutorial_outgoing"),
          timeout: 2**31-1
        });
        break;
      case 6:
        toast.hideSnackbar();
        toast.showSnackbar({
          message: _("elim_action_tutorial_input"),
          timeout: 2**31-1
        });
        break;
      case 7:
        if (this.model.center !== null) {
          this._tutorialStep = 3;
          return this.advanceTutorial();
        }
        this.view.highlightInitial(() => this._tutorialStep === 7);
        this.view.highlightAccepting(() => this._tutorialStep === 7);
        toast.hideSnackbar();
        toast.showSnackbar({
          message: _("elim_action_tutorial_finished"),
          timeout: 2**31-1
        });
        break;
      default:

    }
  }

  selectState(state) {
    if (this.model.selectState(state)) {
      if (this.model.center === null) {
        this.view.drawOverview(this.model.map);
      } else if (this.model.left === null) {
        if (this.model.ins.length === 1 && !this._tutorialStep) {
          return this.selectState(this.model.ins[0]);
        }
        this.view.drawDetail(this.model.map, this.model.ins, this.model.center, this.model.outs);
      } else if (this.model.right === null) {
        if (this.model.outsleft.length === 1 && !this._tutorialStep) {
          return this.selectState(this.model.outsleft[0]);
        }
        this.view.drawDetail(this.model.map, [this.model.left], this.model.center, this.model.outsleft);
      } else {
        this.view.drawDetail(this.model.map, [this.model.left], this.model.center, [this.model.right]);
        this.view.queryRE(this.model.left, this.model.center, this.model.right, this.model.map);
      }
      if (this._tutorialStep) {
        this.advanceTutorial();
      }
      return true;
    }
    return false;
  }

  submitRE(re) {
    if (this.model.offerRE(re)) {
      if (this.model.center === null) {
        this._tutorialStep = (Object.keys(this.model.map).length > 2 ? 1 : 6) * !!this._tutorialStep;
        this.view.drawOverview(this.model.map, this.model.gnfa.getInitialState(), this.model.gnfa.getAcceptingState());
      } else if (this.model.left === null) {
        this._tutorialStep = 3 * !!this._tutorialStep;
        this.view.drawDetail(this.model.map, this.model.ins, this.model.center, this.model.outs);
      } else {
        this._tutorialStep = 4 * !!this._tutorialStep;
        this.view.drawDetail(this.model.map, [this.model.left], this.model.center, this.model.outsleft);
      }
      if (this._tutorialStep) {
        this.advanceTutorial();
      }
      return true;
    }
    return false;
  }
}
