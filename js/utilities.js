/*
 * Copyright 2018 Tom Kranz
 *
 * This file is part of RegApp.
 *
 * RegApp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RegApp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RegApp.  If not, see <https://www.gnu.org/licenses/>.
 */

class Utilities {

constructor(Module) {
  this.Module = Module;
  this.elementsOfAutomaton = this.elementsOfAutomaton.bind(this);
  this.elementsToAutomaton = this.elementsToAutomaton.bind(this);
  this.generateNewState = this.generateNewState.bind(this);
  this.logAutomaton = this.logAutomaton.bind(this);
  this.defaultStyle = this.defaultStyle.bind(this);
}

elementsOfAutomaton(automaton){
  let eles = [];
  if (automaton instanceof this.Module.GNFA) {
    let empty = new this.Module.NFA();
    let states = automaton.getActiveStates();
    states.push_back(automaton.getInitialState());
    states.push_back(automaton.getAcceptingState());
    for (let q = 0; q < states.size(); q++) {
      let qName = states.get(q);
      eles.push({
          group: "nodes",
          data: {
            id: qName,
            label: qName
          },
          classes: qName === automaton.getInitialState() ? "initial" : qName === automaton.getAcceptingState() ? "accepting" : ""
        });
      for (let p = 0; p < states.size(); p++) {
        let pName = states.get(p);
        let edge = automaton.getTransition(qName, pName);
        if (edge.equals(empty)) {
          continue;
        }
        let edgeName = edge.toString();
        eles.push({
          group: "edges",
          data: {
            id: qName+"-"+edgeName+">"+pName,
            label: edgeName,
            source: qName,
            target: pName
          }
        });
        edge.delete();
      }
    }
    empty.delete();
    states.delete();
  } else {
    let deterministic = automaton instanceof this.Module.DFA;
    let alphabet = automaton.getAlphabet();
    let states = automaton.getStates();
    for (let q = 0; q < states.size(); q++) {
      let qName = states.get(q);
      let classes = [];
      if (q === 0) {
        classes.push('initial');
      }
      if (automaton.isAccepting(qName)) {
        classes.push('accepting');
      }
      eles.push({
        group: "nodes",
        data: {
          id: qName,
          label: qName
        },
        "classes": classes.join(' ')
      });
      for (let s = 0; s < alphabet.size(); s++) {
        let symbol = alphabet.get(s);
        if (deterministic){
          let pName = automaton.delta(qName, symbol);
          eles.push({
            group: "edges",
            data: {
              id: qName+"-"+symbol+">"+pName,
              label: symbol,
              source: qName,
              target: pName
            }
          });
        } else {
          let ps = automaton.delta(qName, symbol);
          for (let p = 0; p < ps.size(); p++) {
            let pName = ps.get(p);
            eles.push({
              group: "edges",
              data: {
                id: qName+"-"+symbol+">"+pName,
                label: symbol,
                source: qName,
                target: pName
              }
            });
          }
          ps.delete();
        }
      }
    }
    states.delete();
    alphabet.delete();
  }
  return eles;
}

elementsToAutomaton(eles, strict) {
  function idToLabel (id) {
    for (let i = 0; i < eles.length; i++) {
      if (eles[i].data.id === id) {return eles[i].data.label;}
    }
    return undefined;
  }
  let states = [];
  let alphabet = [];
  let accepting= [];
  let nondeterministic;
  let builder = new this.Module.FABuilder();
  for (let i = 0; i < eles.length; i++) {
    if (eles[i].group === 'nodes') {
      let cl = eles[i].classes;
      let name = eles[i].data.id;
      if (cl.includes('initial')) {
        builder.makeInitial(name);
      }
      builder.setAccepting(name, cl.includes('accepting'));
    } else if (eles[i].group === 'edges') {
      builder.addTransition(eles[i].data.source, eles[i].data.target, eles[i].data.label);
    }
  }
  let automaton;
  if (builder.hasNondeterminism() || (strict && !builder.isComplete())) {
    automaton = builder.buildNFA();
  } else {
    automaton = builder.buildDFA();
  }
  builder.delete();
  return automaton;
}

generateNewState(eles) {
  for (let q = 0;; q++) {
    let exists = false;
    for (let i = 0; i < eles.length; i++) {
      exists |= (eles[i].data.label === "q"+q);
      if (exists) {break;}
    }
    if (!exists) {
      return "q"+q;
    }
  }
}

logAutomaton(automaton){
  let nondeterministic = (automaton instanceof this.Module.NFA);
  let alphabet = automaton.getAlphabet();
  let states = automaton.getStates();
  let line = "";
  for (let i = 0; i < alphabet.size(); i++) {
    line += "\t" + alphabet.get(i);
  }
  console.log(line);
  for (let q = 0; q < states.size(); q++) {
    let qName = states.get(q);
    line = (automaton.isAccepting(qName) ? "*" : " ") + qName;
    for (let s = 0; s < alphabet.size(); s++) {
      if (nondeterministic) {
        let ps = automaton.delta(qName, alphabet.get(s));
        let pNames = [];
        for (let i = 0; i < ps.size(); i++) {
          pNames.push(ps.get(i));
        }
        line += "\t{" + pNames.join(", ") + "}";
        ps.delete();
      } else {
        line += "\t" + automaton.delta(qName, alphabet.get(s));
      }
    }
    console.log(line);
  }
  states.delete();
  alphabet.delete();
}

readAutomaton (spec) {
  var b = new this.Module.FABuilder();
  if (spec.states.length > 0) {
    b.makeInitial(spec.states[0]);
  }
  for (var q = 0; q < spec.states.length; q++) {
    for (var s = 0; s < spec.alphabet.length; s++) {
      let reached = spec.transitions[q][s];
      if (Array.isArray(reached)) {
        reached.forEach(p => b.addTransition(spec.states[q], spec.states[p], spec.alphabet[s]))
      } else {
        b.addTransition(spec.states[q], spec.states[reached], spec.alphabet[s]);
      }
    }
  }
  for (var i = 0; i < spec.accepting.length; i++) {
    b.setAccepting(spec.states[spec.accepting[i]], true);
  }
  return b;
}

defaultStyle() {
  return [
    {
      selector: 'node',
      style: {
        'label': 'data(label)',
        'text-halign': 'center',
        'text-valign': 'center',
        'border-width': '1px',
      }
    },
    {
      selector: "edge",
      style: {
        'curve-style': 'bezier',
        'target-arrow-shape': 'triangle',
        'label': ele=>ele.data("label") && (ele.data("label").length ? ele.data("label") : "ε"),
      }
    },
    {
      selector: ".accepting",
      style: {
        'border-width': '4px',
        'border-style': 'double'
      }
    },
    {
      selector: ".initial",
      style: {
        'background-blacken': -0.75
      }
    },
    {
      selector: ".future",
      style: {
        'background-blacken': -0.75
      }
    },
    {
      selector: ".past",
      style: {
        'shape': 'star'
      }
    },
    {
      selector: ".highlighted",
      style: {
        'background-opacity': 0
      }
    }
  ];
}
}
