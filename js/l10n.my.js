var _ = string => string.toLocaleString();

{
  const storage = window.localStorage;
  const storageKeyLocale = "regweb_locale";
  function applyStrings() {
    document.querySelectorAll('[data-text]').forEach(function(item) {
      var textId = item.getAttribute('data-text');
      item.innerHTML = _(textId);
    });
  }
  function setLocale(l) {
    String.locale = l;
    applyStrings();
    if (storage) {
      storage.setItem(storageKeyLocale, l);
    }
  }
  String.defaultLocale = "en";
  if (storage && storage.getItem(storageKeyLocale)) {
    String.locale = storage.getItem(storageKeyLocale);
  }
}
