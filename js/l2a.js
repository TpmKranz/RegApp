/*
 * Copyright 2018 Tom Kranz
 *
 * This file is part of RegApp.
 *
 * RegApp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RegApp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RegApp.  If not, see <https://www.gnu.org/licenses/>.
 */

var spec;
var solution;
var cy;
var resizeTimeoutId;
var tappedNode;
var recenterAfterEdit;
var grabbedPosition;
var tappedEdge;
var utilities;

reglibjs().then(Module => utilities = new Utilities(Module));

var toast;

function readAutomaton (spec) {
  var b = new utilities.Module.FABuilder();
  if (spec.states.length > 0) {
    b.makeInitial(spec.states[0]);
  }
  for (var q = 0; q < spec.states.length; q++) {
    for (var s = 0; s < spec.alphabet.length; s++) {
      b.addTransition(spec.states[q], spec.states[spec.transitions[q][s]], spec.alphabet[s]);
    }
  }
  for (var i = 0; i < spec.accepting.length; i++) {
    b.setAccepting(spec.states[spec.accepting[i]], true);
  }
  return b;
}

function readFile(files) {
  var file = files[0];
  if (!file) {
    return;
  }
  var reader = new FileReader();
  reader.addEventListener("load", (eve) => {
    spec = JSON.parse(eve.target.result);
    let builder;
    if (spec.handwritten) {
      builder = readAutomaton(spec.handwritten);
    } else if (spec.graph) {
      builder = utilities.elementsToAutomaton(spec.graph);
    } else if (spec.re) {
      let e = utilities.Module.REFromString(spec.re);
      let n = e.asNFA();
      builder = new utilities.Module.FABuilder(n);
      n.delete();
      e.delete();
    } else {
      throw "No solution in input file";
    }
    try {
      builder.minimize();
    } catch (e) {
      utilities.Module.extractMessageNondeterminismException(e);
      builder.powerset();
      builder.minimize();
    }
    builder.normalizeStateNames("q");
    solution = builder.buildDFA();
    builder.delete();
    if (spec.description) {
      let localeObj = {};
      for (let loc in spec.description) {
        if (spec.description.hasOwnProperty(loc)) {
          localeObj[loc] = {};
          localeObj[loc]["l2a_action_help_exercise"] = spec.description[loc];
        }
      }
      String.toLocaleString(localeObj);
      applyStrings();
    }
    initializeWorkspace();
  });
  reader.readAsText(file);
}

function initializeWorkspace() {
  toast = document.getElementById('toast').MaterialSnackbar;
  toast.hideSnackbar();
  cy = cytoscape({
    container: document.getElementById("paper"),
    elements: [{
      group: 'nodes', data: {label: "q0"}, classes: 'initial'
    }],
    layout: {
      name: "cose"
    },
    style: utilities.defaultStyle(),
    autounselectify: true
  });
  cy.on("tap", (eve) => {
    if (eve.target === cy) {
      var newNode = cy.add({
        group: "nodes",
        data: {
          label: utilities.generateNewState(cy.elements().jsons())
        },
        position: eve.position
      });
      editNode(newNode, true);
    }
  });
  cy.on("tap", "node", (eve) => {if (!tappedEdge) {editNode(eve.target);}});
  cy.on("tap", "edge", (eve) => {editEdge(eve.target);});
  cy.on("grabon", "node", (eve) => {
    grabbedPosition = {};
    grabbedPosition.x = eve.target.position().x;
    grabbedPosition.y = eve.target.position().y;
  });
  cy.on("free", "node", (eve) => {
    if (grabbedPosition && !tappedEdge) {
      for (var i = 0; i < cy.elements().length; i++) {
        var ele = cy.elements()[i];
        if (ele === eve.target) {
          continue;
        }
        if (Math.pow(eve.target.position().x - ele.position().x, 2) + Math.pow(eve.target.position().y - ele.position().y, 2) < Math.pow(ele.width()/2, 2) + Math.pow(ele.height()/2, 2)) {
          var newEdge = cy.add({
            group: "edges",
            data: {
              source: eve.target.id(),
              target: ele.id()
            },
          });
          eve.target.animate({position: grabbedPosition});
          editEdge(newEdge, true);
          break;
        }
      }
    }
    grabbedPosition = undefined;
  });
  cy.on("taphold", "node", (eve) => {
    var newEdge = cy.add({
      group: "edges",
      data: {
        source: eve.target.id(),
        target: eve.target.id()
      },
    });
    editEdge(newEdge, true);
  });
  window.addEventListener("resize", (event) => {
    clearTimeout(resizeTimeoutId);
    resizeTimeoutId = setTimeout(() => {
        cy.animate({center:{}, fit:{padding:30}});
      },
      100
    );
  }, false);
  document.querySelectorAll('.notyetvisibile').forEach(
    (item) => {item.classList.remove('notyetvisibile');}
  );
  document.getElementById("action_load").classList.add("notyetvisibile");
}

function editEdge(edge, recenter) {
  tappedEdge = edge;
  recenterAfterEdit = recenter;
  var input = document.getElementById("transition_dialog_trigger");
  input.value = (tappedEdge.data("label") || "");
  input.parentElement.MaterialTextfield.checkDirty();
  input.parentElement.MaterialTextfield.checkValidity();
  document.getElementById("transition_dialog").showModal();
}

function editNode(node, recenter) {
  tappedNode = node;
  recenterAfterEdit = recenter;
  document.getElementById("edit_dialog_delete").disabled = tappedNode.hasClass('initial');
  var input = document.getElementById("edit_dialog_name");
  input.value = (tappedNode.data("label") || "");
  input.parentElement.MaterialTextfield.checkDirty();
  input = document.getElementById("edit_dialog_initial");
  if (tappedNode.hasClass('initial')) {
    input.parentElement.MaterialCheckbox.check();
    input.parentElement.MaterialCheckbox.disable();
  } else {
    input.parentElement.MaterialCheckbox.uncheck();
    input.parentElement.MaterialCheckbox.enable();
  }
  input = document.getElementById("edit_dialog_accepting");
  if (tappedNode.hasClass('accepting')) {
    input.parentElement.MaterialCheckbox.check();
  } else {
    input.parentElement.MaterialCheckbox.uncheck();
  }
  document.getElementById("edit_dialog").showModal();
}

function deleteEdge() {
  if (tappedEdge) {
    tappedEdge.remove();
    recenterAfterEdit = undefined;
    tappedEdge = undefined;
  }
  document.getElementById("transition_dialog").close();
}

function deleteNode() {
  if (tappedNode) {
    tappedNode.remove();
    recenterAfterEdit = undefined;
    tappedNode = undefined;
  }
  document.getElementById("edit_dialog").close();
}

function saveEdge() {
  if (tappedEdge) {
    const input = document.getElementById('transition_dialog_trigger')
    if (input.checkValidity()) {
      tappedEdge.data("label", input.value);
      if (recenterAfterEdit) {
        cy.animate({center:{}, fit:{padding:30}});
      }
      recenterAfterEdit = undefined;
      tappedEdge = undefined;
      document.getElementById("transition_dialog").close();
    }
  } else {
    document.getElementById("transition_dialog").close();
  }
}

function saveNode() {
  if (tappedNode) {
    tappedNode.data("label", document.getElementById("edit_dialog_name").value);
    tappedNode.toggleClass('initial', document.getElementById("edit_dialog_initial").checked);
    if (tappedNode.hasClass('initial')) {
      cy.elements().forEach((ele) => {
        if (ele.id() !== tappedNode.id()) {ele.removeClass('initial');}
      });
    } else {
      var initialExists = false;
      for (var i = 0; i < cy.elements().length; i++) {
        initialExists |= cy.elements()[i].hasClass('initial');
        if (initialExists) {break;}
      }
      if (!initialExists) {
        tappedNode.addClass('initial');
      }
    }
    tappedNode.toggleClass('accepting', document.getElementById("edit_dialog_accepting").checked);
    if (recenterAfterEdit) {
      cy.animate({center:{}, fit:{padding:30}});
    }
    recenterAfterEdit = undefined;
    tappedNode = undefined;
  }
  document.getElementById("edit_dialog").close();
}

function checkSolution() {
  var attempt = utilities.elementsToAutomaton(cy.elements().jsons(), true);
  var data;
  if (attempt instanceof utilities.Module.NFA) {
    data = {message: _("l2a_action_check_nd")};
  } else if (attempt.equals(solution)) {
    let attemptStates = attempt.getStates();
    let solutionStates = solution.getStates();
    var statesTooMany = attemptStates.size() - solutionStates.size();
    attemptStates.delete();
    solutionStates.delete();
    if (statesTooMany) {
      data = {message: _("l2a_action_check_ok_plural1") + statesTooMany + _("l2a_action_check_ok_plural2")};
    } else if (statesTooMany == 1 ) {
      data = {message: _("l2a_action_check_ok_singular1") + statesTooMany + _("l2a_action_check_ok_singular2")};
    } else {
      data = {message: _("l2a_action_check_perfect")};
    }
  } else {
    let empty = new utilities.Module.DFA();
    if (attempt.equals(empty)) {
      data = {message: _("l2a_action_check_wrong_empty")};
    } else {
      let attemptn = attempt.asNFA();
      let solutionn = solution.asNFA();
      let b = new utilities.Module.FABuilder(attemptn);
      b.subtract(solutionn);
      let diff = b.buildDFA();
      let witness;
      if (!diff.equals(empty)) {
        witness = diff.findShortestWord() || "ε";
        data = {message: _("l2a_action_check_wrong_gt1") + witness + _("l2a_action_check_wrong_gt2")};
      } else {
        diff.delete();
        b.delete();
        b = new utilities.Module.FABuilder(solutionn);
        b.subtract(attemptn);
        diff = b.buildDFA();
        if (!diff.equals(empty)) {
          witness = diff.findShortestWord() || "ε";
          data = {message: _("l2a_action_check_wrong_lt1") + witness + _("l2a_action_check_wrong_lt2")};
        } else {
          data = {message: _("l2a_action_check_wrong")};
        }
      }
      diff.delete();
      b.delete();
      attemptn.delete();
      solutionn.delete();
    }
    empty.delete();
  }
  attempt.delete();
  if (toast.active) {
    toast.hideSnackbar();
    setTimeout(showToastFn(data), toast.Constant_.ANIMATION_LENGTH);
  } else {
    showToastFn(data)();
  }
}

function showToastFn(data) {
  return () => {
    toast.showSnackbar(data);
    toast.textElement_.innerHTML = data.message;
  }
}
