RegApp
===
An app for visualizing the concepts behind regular language formalisms.

Try https://regapp.tomkra.nz for a live version.

### Setup
0. Clone this repo:
   ```bash
   git clone https://gitlab.com/TpmKranz/RegApp.git /path/to/RegApp
   ```
3. Create your build directory and go there:
   ```bash
   mkdir /path/to/build && cd /path/to/build
   ```
4. Run CMake, informing it from where the web app will be served:
   ```bash
   cmake -DCMAKE_INSTALL_PREFIX=/path/to/www /path/to/RegApp
   ```
5. Run Make to install the web app to the server directory:
   ```bash
   make install
   ```
6. Serve the web app, for example (with [emsdk](https://emscripten.org) in `PATH`):
   ```bash
   emrun /path/to/www/index.html
   ```
