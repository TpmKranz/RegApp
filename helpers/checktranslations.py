#!/usr/bin/env python3
# Copyright 2018-2019 Tom Kranz
#
# This file is part of RegApp.
#
# RegApp is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# RegApp is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with RegApp.  If not, see <https://www.gnu.org/licenses/>.

import sys
import json

strings = list(sys.stdin.read().splitlines())

for localization in sys.argv[1:]:
    locDict = {}
    with open(localization, 'rt') as locFile:
        locDict = json.loads(''.join(locFile.readlines()[1:-1]))
        for locale in locDict:
            for string in strings:
                if string not in locDict[locale]:
                    locDict[locale][string] = string + "#NEEDSTRANSLATING"
                    print("+", locale, string)
            for string in locDict[locale]:
                if string not in strings:
                    locDict[locale][string] += "#UNUSED"
                    print("-", locale, string)
    with open(localization, 'wt') as locFile:
        print("String.toLocaleString(", file=locFile)
        print(json.dumps(locDict, sort_keys=True, indent=2, ensure_ascii=False), file=locFile)
        print(");", file=locFile)
